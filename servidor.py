import socket
import sys 

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
puerto = 8080
direccion = ("localhost",puerto)
print('Iniciando servidor en el puerto: ' + str(puerto))
sock.bind(direccion)
sock.listen(1)

while True:
    print('Esperando las conexiones')
    connection, direccioncliente = sock.accept()
    try:
        print('Conexión entrante de ' + str(direccioncliente))
        while True:
            data = connection.recv(16)
            print('received{!r}'.format(data))
            if data:
                connection.sendall(data)
            else:
                print('!data', direccioncliente)
                break
    finally:
        connection.close()
