import socket
import sys

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ip = input('Ingrese la direcion del servidor: ')
puerto = input('ingrese el puerto: ')
direccion = (ip, puerto)
print('conectando a {} puerto {}'.format(*direccion))
sock.connect(direccion)

try:
    message = b'Mensaje'
    print('enviando... {!r}'.format(message))
    sock.sendall(message)

    amount_received = 0
    amount_expected = len(message)

    while amount_received < amount_expected:
        data = sock.recv(16)
        amount_received += len(data)
        print('received{!r}'.format(data))

finally:
    sock.close()